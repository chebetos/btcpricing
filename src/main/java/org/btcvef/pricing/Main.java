package org.btcvef.pricing;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.Collection;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Main {

	public static void main(String[] args) throws Exception {
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("localhost", 1080));
		
//		Authenticator authenticator = new Authenticator() {
//	        public PasswordAuthentication getPasswordAuthentication() {
//	            return (new PasswordAuthentication("X88411DE",
//	                    "87654321".toCharArray()));
//	        }
//	    };
//	    Authenticator.setDefault(authenticator);
	    
		String tradeString = getContentFromURL("https://localbitcoins.com/bitcoincharts/VEF/trades.json", proxy);
		System.out.printf("tradeString: %s\n", tradeString);

		String orderBookString = getContentFromURL("https://localbitcoins.com/bitcoincharts/VEF/orderbook.json", proxy);
		System.out.printf("orderBookString: %s\n", orderBookString);

		Gson gson = new Gson();
		Type tradeCollectionType = new TypeToken<Collection<Trade>>(){}.getType();
		Collection<Trade> trades = gson.fromJson(tradeString, tradeCollectionType);

		Orderbook orderBook = gson.fromJson(orderBookString, Orderbook.class);

		
		//System.out.printf("trades: %s\n", trades);
		int i = 0;
		for (Trade trade : trades) {
			System.out.printf("%d.- trade: %s\n", i++, trade);
		}
		
		System.out.printf("Orderbook: %s\n", orderBook);
	}

	private static String getContentFromURL(String sUrl, Proxy proxy)
			throws MalformedURLException, IOException {
		URL url = new URL(sUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);
		InputStream is = connection.getInputStream();
		String content = IOUtils.toString(is);
		is.close();
		return content;
	}

}
