package org.btcvef.pricing;

import java.math.BigDecimal;
import java.util.Map;

public class Orderbook {

	private Map<BigDecimal, BigDecimal> bids;

	private Map<BigDecimal, BigDecimal> asks;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Orderbook [bids=\n");
		int i = 0;
		for (Map.Entry<BigDecimal, BigDecimal> bidEntry : this.bids.entrySet()) {
			builder.append(String.format("\t%d.- %s: \t%s\n", i++, bidEntry.getKey(),
					bidEntry.getValue()));
		}
		builder.append(", asks=\n");
		int j = 0;
		for (Map.Entry<BigDecimal, BigDecimal> askEntry : this.asks.entrySet()) {
			builder.append(String.format("\t%d.-%s: \t%s\n", j++, askEntry.getKey(),
					askEntry.getValue()));
		}
		builder.append("]");
		return builder.toString();
	}

	public Map<BigDecimal, BigDecimal> getBids() {
		return bids;
	}

	public void setBids(Map<BigDecimal, BigDecimal> bids) {
		this.bids = bids;
	}

	public Map<BigDecimal, BigDecimal> getAsks() {
		return asks;
	}

	public void setAsks(Map<BigDecimal, BigDecimal> asks) {
		this.asks = asks;
	}

}
