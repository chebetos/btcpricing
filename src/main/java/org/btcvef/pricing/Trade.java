package org.btcvef.pricing;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Date;

public class Trade {

	private int tid;

	private long date;

	private BigDecimal amount;

	private BigDecimal price;

	public int getTid() {
		return tid;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Trade [tid=");
		builder.append(tid);
		builder.append(", date=");
		builder.append(date);
		builder.append("=");
		builder.append(DateFormat.getInstance().format(getDateAsDate()));
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", price=");
		builder.append(price);
		builder.append("]");
		return builder.toString();
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public Date getDateAsDate() {
		return new Date(this.date * 1000);
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
